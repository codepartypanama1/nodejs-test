const koa = require("koa")
const koaRouter = require("koa-router")
const koaMorgan = require("koa-morgan")
const bodyParser = require("koa-bodyparser")
const json = require("koa-json")
const pug = require("pug")
const mu = require("mu2")

const app = koa()

app.use(koaMorgan.middleware('combined'))
app.use(json())
app.use(bodyParser())

const main_router = new koaRouter()
main_router.get("/", function*(){
	this.body = pug.renderFile("./templates/index.pug")
})
main_router.get("/hello", function*(){
	this.body = "XOPAAAAAAA"
})
main_router.get("/hello/:nombre", function*(){
	let nombre = this.params.nombre
	this.body = pug.renderFile("./templates/hola.pug", {
		nombre: nombre
	})
})

app.use(main_router.routes())

app.listen(8080, function(){
	console.log("Hello World en puerto 8080")
})
